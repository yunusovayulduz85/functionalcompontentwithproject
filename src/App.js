// import React, { Component, useState } from 'react';
// // rafc
// function App() {
//   const [count, setCount] = useState(0);
//   const [counter, setCounter] = useState([]);
//   const add = (index) => {
//     counter[index]+=1;
//     setCounter([...counter]);
//   }
//   const separate = (index) => {
//     counter[index] -= 1;
//     setCounter([...counter])
//   }
//   const addedCounter = () => {
//     setCounter(value => [...value,0]);
//   }
//   return <>
//     <button  onClick={addedCounter} > Add Counter</button >
//     {
//       counter.map((item,index) => {
//         return <div key={index}>
//           <span>
//             {index+1} )  counter{index+1} : {item} <button onClick={()=>add(index)}>+</button>  <button onClick={()=>separate(index)}>-</button>
//           </span>
//         </div>
//       })
//     }
//   </>
// }

// export default;
import React,{Component,useState} from "react";
import UserIcon from "./assets/user-minus-solid.svg";
function Head(){
 
  return(
    <>
    <table className="table mt-4">
      <tr className="bg-secondary">
        <td className="p-3">#</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Phone</td>
        <td>Active</td>
        <td>Count</td>
        <td>Action</td>
      </tr>
      {Body()}
    </table>
    </>
  )
}
function Body(){
  var [lists, setLists] = useState([]);
  const addedList = () => {
    setLists(value => [...value,2])
  }
  const addNumber=(index)=>{
    lists[index]+=1;
    setLists([...lists]);
  }
  const removeNumber=(index)=>{
    lists[index]-=1;
    setLists([...lists]);
  }
  const [a,setA]=useState(16)
  const [b,setB]=useState(4)
  const [c,setC]=useState(2)
  const [defaultList,setDefaultList]=useState([])
  const [order,setOrder]=useState(1)
  const oneAdd=()=>{
    setA(a=>a+=1) 
  }
  const oneRemov=()=>{
    setA(a=>a-=1)
  }
  const twoAdd=()=>{
    setB(b=>b+=1) 
  }
  const twoRemove=()=>{
    setB(b=>b-=1)
  }
  const threeAdd=()=>{
    setC(c=>c+=1) 
  }
  const threeRemove=()=>{
    setC(c=>c-=1)
  }
  const removeList=(index)=>{
    lists.splice(index,1)
    setLists([...lists]);
  }
  // const removeUser=document.querySelector(".removeUser");
  // const oneTr=document.querySelector(".oneTr");
  // removeUser.addEventListener("click",()=>{
    // oneTr.classList.toggle("removed");
  // })
  return <>
  <tr className="m-2 oneTr">
    <td>{order}</td>
    <td>Mark</td>
    <td>Otto</td>
    <td>+998941211112
    </td>
    <td><input type="checkbox"/></td>
    <td>
      <button className="btn btn-secondary" onClick={oneAdd}>+</button>
      <span>{a}</span>
        <button className="btn btn-secondary" onClick={oneRemov}>-</button>
    </td>
      <td> <img src={UserIcon} alt="user remove icon" width={"50px"} style={{ cursor: "pointer" }} className="removeUser"/>
      </td>
  </tr>
    <tr className="m-2">
      <td>2</td>
      <td>Jacob</td>
      <td>Throton</td>
      <td>+998941211113
      </td>
      <td><input type="checkbox" /></td>
      <td>
        <button className="btn btn-secondary" onClick={twoAdd}>+</button>
        <span>{b}</span>
        <button className="btn btn-secondary" onClick={twoRemove}>-</button>
      </td>
      <td> <img src={UserIcon} alt="user remove icon" width={"50px"} style={{ cursor: "pointer" }} />
      </td>
    </tr>
    <tr className="m-2">
      <td>3</td>
      <td>Larry</td>
      <td>theBird</td>
      <td>+998941211111
      </td>
      <td><input type="checkbox" /></td>
      <td>
        <button className="btn btn-secondary" onClick={threeAdd}>+</button>
        <span>{c}</span>
        <button className="btn btn-secondary" onClick={threeRemove}>-</button>
      </td>
      <td> <img src={UserIcon} alt="user remove icon" width={"50px"} style={{cursor:"pointer"}}/>
      </td>
    </tr>
    {
      lists.map((item,index)=>{
       return <tr key={index}>
          <td>{index+4}</td>
          <td>Larry</td>
          <td>theBird</td>
          <td>+998941211111
          </td>
          <td><input type="checkbox" /></td>
          <td>
            <button className="btn btn-secondary" onClick={()=>addNumber(index)}>+</button>
            <span>{item}</span>
            <button className="btn btn-secondary" onClick={()=>removeNumber(index)}>-</button>
          </td>
          <td> <img src={UserIcon} alt="user remove icon" width={"50px"} style={{cursor:"pointer"}} onClick={()=>removeList(index)}/>
          </td>
        </tr>
      })
    }
    <button onClick={addedList} className="btn btn-info">Add</button>
  </>
}
export default Head;